#include "Seven_segment_display.h"
#include "arduino_debug.h"
#include "seven_segment_characters.h"
#include "temperature_sensor.h"

#include <Arduino.h>
#include <DHT.h>

void setup()
{
#if defined(ARDUINO_DEBUG) && defined(HAS_SERIAL)
    Serial.begin(BAUD_RATE);
#endif

    Seven_segment_display::setup_seven_segment_display();

    // Setup temperature sensor
    g_dht.begin();
}

void loop()
{
    Seven_segment_display::get_instance().display_cycle();
    delay(1000);

    float temp = get_temperature(true);

    Seven_segment_display::get_instance().display_float(temp);

    delay(5000);

    float humidity = get_humidity();

    Seven_segment_display::get_instance().display_float(humidity);

    delay(5000);
}
