#pragma once

#include <DHT.h>

extern DHT g_dht;

float get_temperature(bool fahrenheit = false);

float get_humidity();
